all:
	rm -rf _site/
	sudo docker run --rm \
		--volume="$(PWD):/srv/jekyll" \
		-it jekyll/jekyll \
		jekyll build

serve:
	cd _site/ &&\
		sudo python3 -m http.server 80

